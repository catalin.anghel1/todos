'use strict';
require('dotenv').config({path: './.env'});
const {User} = require('../db/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const SECRET_KEY = process.env.SECRET;

exports.signUp = (req, res) => {
    const {name, email, password} = req.body;
    bcrypt.hash(password, 10, (err, hash) => {
        if (err) {
            return res.status(500).json({
                error: err,
            });
        } else {
            User.create({name, email, password: hash})
                .then(() => {
                    res.status(201).json({
                        message: 'User created.',
                    });
                })
                .catch((error) => {
                    res.status(500).json({
                        error: error.message,
                    });
                });
        }
    });
};

exports.login = (req, res) => {
    const {email, password} = req.body;
    User.findOne({where: {email}})
        .then((user) => {
            if (user) {
                bcrypt.compare(password, user.password, (err, result) => {
                    if (err) {
                        return res.status(401).json({
                            message: 'Authentication failed.',
                        });
                    }
                    if (result) {
                        const token = jwt.sign(
                            {
                                email: user.email,
                                userId: user.id,
                            },
                            SECRET_KEY,
                            {expiresIn: '1h'},
                        );
                        return res.status(200).json({
                            message: 'Authentication successful.',
                            user: {
                                email: user.email,
                                name: user.name,
                                userId: user.id,
                            },
                            token,
                        });
                    }
                    return res.status(401).json({
                        message: 'Authentication failed.',
                    });
                });
            } else {
                return res.status(401).json({
                    message: 'Authentication failed.',
                });
            }
        })
        .catch((error) => {
            res.status(500).json({
                error: error.message,
            });
        });
};
