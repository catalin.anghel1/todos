const express = require('express');
const router = express.Router();

const { createTodo, markTodoCompleted, markTodoUncompleted, deleteTodo, listTodos } = require('./todos');
const { signUp, login } = require('./auth');
const { authenticate } = require('../middleware/index');

// todos routes
router.post('/todos', authenticate, createTodo);
router.put('/todos/:id/completed', authenticate, markTodoCompleted);
router.put('/todos/:id/uncompleted', authenticate, markTodoUncompleted);
router.delete('/todos/:id', authenticate, deleteTodo);
router.get('/todos', authenticate, listTodos);

// auth routes
router.post('/signup', signUp);
router.post('/login', login);

module.exports = router;