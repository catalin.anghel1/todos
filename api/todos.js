'use strict';
const {Todo} = require('../db/index');

exports.createTodo = (req, res) => {
    const {title} = req.body;
    Todo.create({title, completed: false, UserId: req.userData.userId})
        .then(() => {
            res.status(201).json({
                message: 'Todo created.',
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: error.message,
            });
        });
};

exports.markTodoCompleted = (req, res) => {
    const {id} = req.params;
    Todo.update({completed: true}, {where: {id, UserId: req.userData.userId}})
        .then(([rowsUpdated]) => {
            if (rowsUpdated === 0) {
                return res.status(404).json({
                    message: 'Todo not found.',
                });
            }
            res.status(200).json({
                message: 'Todo updated.',
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: error.message,
            });
        });
};

exports.markTodoUncompleted = (req, res) => {
    const {id} = req.params;
    Todo.update({completed: false}, {where: {id, UserId: req.userData.userId}})
        .then(([rowsUpdated]) => {
            if (rowsUpdated === 0) {
                return res.status(404).json({
                    message: 'Todo not found.',
                });
            }
            res.status(200).json({
                message: 'Todo updated.',
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: error.message,
            });
        });
};

exports.deleteTodo = (req, res) => {
    const {id} = req.params;
    Todo.destroy({where: {id, UserId: req.userData.userId}})
        .then((rowsDeleted) => {
            if (rowsDeleted === 0) {
                return res.status(404).json({
                    message: 'Todo not found.',
                });
            }
            res.status(200).json({
                message: 'Todo deleted.',
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: error.message,
            });
        });
};

exports.listTodos = (req, res) => {
    Todo.findAll({where: {UserId: req.userData.userId}})
        .then((todos) => {
            res.status(200).json({
                todos,
            });
        })
        .catch((error) => {
            res.status(500).json({
                error: error.message,
            });
        });
};
