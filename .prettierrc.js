module.exports = {
    printWidth: 120,
    tabWidth: 4,
    singleQuote: true,
    trailingComma: 'all',
    bracketSpacing: false,
    arrowParens: 'always',
    overrides: [
        {
            files: ['.*rc'
            ],
            options: {
                tabWidth: 2,
            },
        },
    ],
};
