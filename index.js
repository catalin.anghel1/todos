'use strict';
require('dotenv').config({path: './.env'});
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./api/routes');
const {sequelize} = require('./db/index');

const app = express();
app.use(
    cors({
        origin: process.env.ORIGIN,
        optionsSuccessStatus: 200,
    }),
);
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/', routes);

sequelize.sync().then(() => {
    console.log('Database and tables created.');

    const PORT = process.env.PORT;
    app.listen(PORT, () => {
        console.log(`Server started on port ${PORT}.`);
    });
});
