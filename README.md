## Instructions

- In order to have the server working, you have to create a .env file in the root of the project
- The .env file should contain 
    - PORT=8080 (this port is used by the web app to make the requests)
    - SECRET - this is a string key used by JWT to sign the token that will be used by the web app for all the requests
    - ORIGIN - the location of the web app (ex: http://localhost:3000)